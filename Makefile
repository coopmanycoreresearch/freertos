IMPERAS_HOME := $(shell getpath.exe "$(IMPERAS_HOME)")

include ../Makefile.common

include $(IMPERAS_HOME)/bin/Makefile.include

ifeq ($(MAPPING),NN)
  DEFINES += -DNN
endif

ifeq ($(MAPPING),LECDN)
  DEFINES += -DLECDN
endif

ifeq ($(PREMAP),1)
  DEFINES += -DPREMAP
endif

ifeq ($(shell test $(OVP_DBG) -gt 0; echo $$?),0)
  DEFINES += -DOVP_DEBUG -DDEBUG=$(OVP_DBG)
endif

ifneq ($(CPUS),1)
  DEFINES += -DMULTIPROCESSOR
endif

FREERTOS_ROOT?=${FREERTOS_FOLDER}

IMPERAS_ERROR := $(if $(wildcard $(FREERTOS_ROOT)*),, $(error "ERROR: Please set FREERTOS_ROOT to FreeRTOS Source installation root."))

CROSS=ARM_CORTEX_$(PROCESSOR)

APP=FreeRTOSDemo
APPELF=$(APP).$(CROSS).elf
APPELFOD=$(APP).$(CROSS).od
APPO=$(APP).$(CROSS).o

FREERTOS_SOURCE=$(FREERTOS_ROOT)
FREERTOS_DEMO_COMMON=$(FREERTOS_ROOT)/Demo/Common

ifneq ($(CPUS),1)
	CSRC=$(wildcard ${EXTENSION_FOLDER}/*.c) \
		 $(wildcard ${CMSIS_FOLDER}/Device/ARM/ARMC$(PROCESSOR)/Source/*.c)
else
	CSRC=$(wildcard ${EXTENSION_FOLDER}/main.c) \
		 $(wildcard ${CMSIS_FOLDER}/Device/ARM/ARMC$(PROCESSOR)/Source/*.c)
endif

PCSRC=$(wildcard $(FREERTOS_SOURCE)/*.c) \
      $(wildcard $(FREERTOS_SOURCE)/portable/MemMang/heap_4.c) \
      $(wildcard $(FREERTOS_SOURCE)/portable/GCC/ARM_C$(PROCESSOR)/port.c)

APPSRC=$(CAPPSRC) $(SAPPSRC)

APPOBJ=$(patsubst $(FREERTOS_ROOT)/%.c,Build/obj/%.o,$(PCSRC)) \
       $(patsubst $(FREERTOS_ROOT)/%.s,Build/obj/%.o,$(PSSRC)) \
       $(patsubst %.c,Build/obj/%.o,$(CSRC)) \
       $(patsubst %.s,Build/obj/%.o,$(SSRC))

ENTRY_RTOSDemo=__Vectors
CFLAGS+=-nostartfiles -lc -lm -D inline= --entry $(ENTRY_RTOSDemo)

APPDBG+= -w $(DEFINES)
APPOPT+= -Os $(APPDBG)

$(CROSS)_LINKER_SCRIPT=-T Imperas.ld
-include $(IMPERAS_HOME)/lib/$(IMPERAS_ARCH)/CrossCompiler/$(CROSS).makefile.include

all: clean $(APPELF) $(APPO) lst_maker

$(APPELF): $(APPOBJ)
	$(V) echo "# Linking $@"
	$(V) mkdir -p $(dir $@)
	$(V) $(IMPERAS_LINK) $(IMPERAS_LDFLAGS) $(CFLAGS) -o $@ $^ $(APPDBG)
	
$(APPO): $(APPOBJ)
	$(V) echo "# Linking $@"
	$(V) mkdir -p $(dir $@)
	$(V) $(IMPERAS_LINK) $(IMPERAS_LDFLAGS) $(CFLAGS) -o $@ $^ $(APPDBG)


Build/obj/%.o: $(FREERTOS_ROOT)/%.c
	$(V)  echo "# Compiling $<"
	$(V) mkdir -p $(dir $@)
	$(V) $(IMPERAS_CC) $(APPINCLUDE) -c $(CFLAGS) $(IMPERAS_CFLAGS) -D$(CROSS) -o $@ $< $(APPOPT)

Build/obj/%.o: $(FREERTOS_ROOT)/%.s
	$(V) echo "# Assembling $<"
	$(V) mkdir -p $(dir $@)
	$(V) $(IMPERAS_AS) $(APPINCLUDE) -o $@ $<  $(APPDBG)

Build/obj/%.o: %.c
	$(V) echo "# Compiling $<"
	$(V) mkdir -p $(dir $@)
	$(V) $(IMPERAS_CC) $(APPINCLUDE) -c $(IMPERAS_CFLAGS) -D$(CROSS) -o $@ $< $(APPOPT)

Build/obj/%.o: %.s
	$(V)    echo "# Assembling $<"
	$(V) mkdir -p $(dir $@)
	$(V) $(IMPERAS_AS) $(APPINCLUDE) -o $@ $< $(APPDBG)

lst_maker:
	$(V) echo "DBG Application"
	$(V) $(IMPERAS_OBJDUMP) --insn-width=4 -j .text -j .init -j .fini -j .rodata -j .data -j .bss -d FreeRTOSDemo.$(CROSS).elf > app.lst
	$(V) $(IMPERAS_OBJCOPY) -S FreeRTOSDemo.$(CROSS).elf -O binary FreeRTOSDemo.$(CROSS).bin
	$(V) $(IMPERAS_OBJCOPY) -S FreeRTOSDemo.$(CROSS).elf -O verilog FreeRTOSDemo.$(CROSS).hex

clean::
	$(V) rm -f $(APPOBJ) $(APPELF) $(APPO)
	$(V) rm -rf Build
