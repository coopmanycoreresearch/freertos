/*
 * FreeRTOS Kernel V10.2.0
 * Copyright (C) 2019 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://www.FreeRTOS.org
 * http://aws.amazon.com/freertos
 *
 * 1 tab == 4 spaces!
 */


#ifndef FREERTOS_CONFIG_H
#define FREERTOS_CONFIG_H

/*-----------------------------------------------------------
 * Application specific definitions.
 *
 * These definitions should be adjusted for your particular hardware and
 * application requirements.
 *
 * THESE PARAMETERS ARE DESCRIBED WITHIN THE 'CONFIGURATION' SECTION OF THE
 * FreeRTOS API DOCUMENTATION AVAILABLE ON THE FreeRTOS.org WEB SITE.
 *
 * See http://www.freertos.org/a00110.html.
 *----------------------------------------------------------*/
/*
 * Miscelaneous particular functions.
 * This library includes clean commom functions such as:
 *  memcpy and memset.
 * */

#include "misc.h"
#include "architecture_include.h"

extern uint32_t SystemCoreClock;

/* Normal assert() semantics without relying on the provision of an assert.h
   header file. */
#if ( DEBUG > 2 )
#define configASSERT( x )     if( ( x  ) == 0  ) ({\
        taskDISABLE_INTERRUPTS();\
        printf("PE (%2d) : #%u : %s, %d\n", PROCESSOR_ID, xTaskGetTickCount(), __FILE__, __LINE__);\
        });
#endif

#define configCPU_CLOCK_HZ           ( SystemCoreClock )

#ifdef RTL
#ifdef CORTEX_M0
#define configTICK_RATE_HZ           ( ( portTickType) 10000 )
#endif
#ifdef CORTEX_M3
#define configTICK_RATE_HZ           ( ( portTickType) 5000 )
#endif
#endif
#ifdef OVP
#define configTICK_RATE_HZ           ( ( portTickType) 10000 )
#endif
#define configTOTAL_HEAP_SIZE        ( ( size_t) ( 0xCFC0 ) ) //around 36kB
#define configMINIMAL_STACK_SIZE     ( ( uint8_t ) 59 )
#define configTASK_STACK_SIZE        ( ( uint8_t ) 128 )
#define configMAX_TASK_NAME_LEN               16
#define configMAX_PRIORITIES                  5
#define configKERNEL_INTERRUPT_PRIORITY       255

/*
 *  configDEFINES options:
 * Example: configMAX_SYSCALL_INTERRUPT_PRIORITY
 *  191 is equivalent to priority 5.
 * More info at:
 * http://www.freertos.org/RTOS-Cortex-M3-M4.html
 */
#define configMAX_SYSCALL_INTERRUPT_PRIORITY  191
#define configSUPPORT_DYNAMIC_ALLOCATION      1
#define configCHECK_FOR_STACK_OVERFLOW        1
#define configIDLE_SHOULD_YIELD               1
#define configUSE_PREEMPTION                  1
#define configUSE_STATS_FORMATTING_FUNCTIONS  1
#define configUSE_MALLOC_FAILED_HOOK          1
#define configUSE_IDLE_HOOK                   0
#define configUSE_TICKLESS_IDLE               0
#define configUSE_TICK_HOOK                   0
#define configUSE_TRACE_FACILITY              0
#define configUSE_16_BIT_TICKS                0

/* Set the following definitions to 1 to include the API function, or zero
to exclude the API function. */
#define INCLUDE_vTaskPrioritySet           0
#define INCLUDE_vTaskPriorityGet           0
#define INCLUDE_uxTaskPriorityGet          0
#define INCLUDE_vTaskDelete                1
#define INCLUDE_vTaskCleanUpResources      1
#define INCLUDE_vTaskSuspend               1
#define INCLUDE_vTaskDelayUntil            0
#define INCLUDE_vTaskDelay                 0
#define INCLUDE_eTaskGetState              1
#define INCLUDE_pcTaskGetTaskName          0
#define INCLUDE_xTaskGetCurrentTaskHandle  1
#define INCLUDE_xTaskGetTickCountFromISR   0

/**
 * Adapted defines
 *
 * **/

#define configUSE_CO_ROUTINES               0
#define configUSE_COUNTING_SEMAPHORES       0
#define configUSE_EVENT_GROUPS              0
#define configUSE_MUTEXES                   0
#define configUSE_QUEUES                    0
#define configUSE_STREAM_BUFFER             0
#define configUSE_TASK_NOTIFICATIONS        0
#define portCRITICAL_NESTING_IN_TCB         0

#endif /* FREERTOS_CONFIG_H */
