/*
	FreeRTOS V8.1.2 - Copyright (C) 2014 Real Time Engineers Ltd.
	All rights reserved

	VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

	***************************************************************************
	 *                                                                       *
	 *    FreeRTOS provides completely free yet professionally developed,    *
	 *    robust, strictly quality controlled, supported, and cross          *
	 *    platform software that has become a de facto standard.             *
	 *                                                                       *
	 *    Help yourself get started quickly and support the FreeRTOS         *
	 *    project by purchasing a FreeRTOS tutorial book, reference          *
	 *    manual, or both from: http://www.FreeRTOS.org/Documentation        *
	 *                                                                       *
	 *    Thank you!                                                         *
	 *                                                                       *
	***************************************************************************

	This file is part of the FreeRTOS distribution.

	FreeRTOS is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License (version 2) as published by the
	Free Software Foundation >>!AND MODIFIED BY!<< the FreeRTOS exception.

	>>!   NOTE: The modification to the GPL is included to allow you to     !<<
	>>!   distribute a combined work that includes FreeRTOS without being   !<<
	>>!   obliged to provide the source code for proprietary components     !<<
	>>!   outside of the FreeRTOS kernel.                                   !<<

	FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
	WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
	FOR A PARTICULAR PURPOSE.  Full license text is available from the following
	link: http://www.freertos.org/a00114.html

	1 tab == 4 spaces!

	***************************************************************************
	 *                                                                       *
	 *    Having a problem?  Start by reading the FAQ "My application does   *
	 *    not run, what could be wrong?"                                     *
	 *                                                                       *
	 *    http://www.FreeRTOS.org/FAQHelp.html                               *
	 *                                                                       *
	***************************************************************************

	http://www.FreeRTOS.org - Documentation, books, training, latest versions,
	license and Real Time Engineers Ltd. contact details.

	http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
	including FreeRTOS+Trace - an indispensable productivity tool, a DOS
	compatible FAT file system, and our tiny thread aware UDP/IP stack.

	http://www.OpenRTOS.com - Real Time Engineers ltd license FreeRTOS to High
	Integrity Systems to sell under the OpenRTOS brand.  Low cost OpenRTOS
	licenses offer ticketed support, indemnification and middleware.

	http://www.SafeRTOS.com - High Integrity Systems also provide a safety
	engineered and independently SIL3 certified version for use in safety and
	mission critical applications that require provable dependability.

	1 tab == 4 spaces!
*/

#ifndef ISR_SUPPORT_H
#define	ISR_SUPPORT_H

/* This file is only used by the port_asm.S file, and so is pretty much a
no-op if (accidentally) included within any other non-assembly files */
#if defined(__ASSEMBLER__)

#include <mips/hal.h>

#include "FreeRTOSConfig.h"

/* Define the context sizes and locations needed for quick access. */
#ifdef __mips_dsp
#define portCONTEXT_SIZE ( CTX_SIZE + DSPCTX_SIZE + FPU_ADJ )
#else
#define portCONTEXT_SIZE ( CTX_SIZE + FPU_ADJ )
#endif

#ifdef __mips_hard_float
#define FPU_ADJ			8
#define FPU_CTX			( portCONTEXT_SIZE - 4 )
#else
#define FPU_ADJ			0
#endif

/* Save this processor context. This will happen when a timer tick happens.
As we are using Count/Compare as our timer, this fires on Status(HW5). */
.macro  portSAVE_CONTEXT

	/* Make room for the context. First save the current status so it can be
	manipulated, and the cause and EPC registers so their original values are
	captured. */
	mfc0        k0, C0_CAUSE
	addiu       sp, sp, -portCONTEXT_SIZE
	mfc0        k1, C0_STATUS
#ifdef __mips_hard_float
	/* disable FPU so that we can lazy context switch */
	ins			k1, zero, SR_CU1_SHIFT, 1
	mtc0        k1, C0_STATUS
	ehb
#endif

	/* Also save s6 and s5 so they can be used.  Any nesting interrupts should
	maintain the values of these registers across the ISR. */
	sw          s6, CTX_S6(sp)
	sw          s5, CTX_S5(sp)
	sw          k1, CTX_STATUS(sp)

	/* Mask out the timer interrupt so that we don't hit an infinite recursion
	when we re-enable interrupts. */
	ins         k0, zero, 15, 1
	ins         k1, k0, 8, 8
	ins         k1, zero, 1, 4

	/* s5 is used as the frame pointer. */
	add         s5, zero, sp

	/* Check the nesting count value. */
	la          k0, uxInterruptNesting
	lw          s6, (k0)

	/* If the nesting count is 0 then swap to the the system stack, otherwise
	the system stack is already being used. */
	bne         s6, zero, 1f
	nop

	/* Swap to the system stack. */
	la          sp, xISRStackTop
	lw          sp, (sp)

	/* Increment and save the nesting count. */
1:  addiu       s6, s6, 1
	sw          s6, 0(k0)

	/* s6 holds the EPC value, this is saved after interrupts are re-enabled. */
	mfc0        s6, C0_EPC

	/* Re-enable interrupts. */
	mtc0        k1, C0_STATUS

	/* Save the context into the space just created.  s6 is saved again
	here as it now contains the EPC value.  No other s registers need be
	saved. */
	sw          ra, CTX_RA(s5)
	sw          s8, CTX_FP(s5)
	sw          t9, CTX_T9(s5)
	sw          t8, CTX_T8(s5)
	sw          t7, CTX_T7(s5)
	sw          t6, CTX_T6(s5)
	sw          t5, CTX_T5(s5)
	sw          t4, CTX_T4(s5)
	sw          t3, CTX_T3(s5)
	sw          t2, CTX_T2(s5)
	sw          t1, CTX_T1(s5)
	sw          t0, CTX_T0(s5)
	sw          a3, CTX_A3(s5)
	sw          a2, CTX_A2(s5)
	sw          a1, CTX_A1(s5)
	sw          a0, CTX_A0(s5)
	sw          v1, CTX_V1(s5)
	sw          v0, CTX_V0(s5)
	sw          s6, CTX_EPC(s5)
	sw          $1, CTX_AT(s5)

	/* s6 is used as a scratch register. */
#if (__mips_isa_rev < 6)
	mfhi        s6
	sw          s6, CTX_HI0(s5)
	mflo        s6
	sw          s6, CTX_LO0(s5)
#endif

#ifdef __mips_hard_float
	la          s6, currentfpctx
	lw          s6, (s6)
	sw			s6, (FPU_CTX + CTX_SIZE + DSPCTX_SIZE)(s5)
#endif

#ifdef __mips_dsp
	/* Store the DSP context */

	rddsp       s6
	sw          s6, (DSPCTX_DSPC + CTX_SIZE)(s5)
	mfhi        s6, $ac3
	sw          s6, (DSPCTX_HI3 + CTX_SIZE)(s5)
	mflo        s6, $ac3
	sw          s6, (DSPCTX_LO3 + CTX_SIZE)(s5)
	mfhi        s6, $ac2
	sw          s6, (DSPCTX_HI2 + CTX_SIZE)(s5)
	mflo        s6, $ac2
	sw          s6, (DSPCTX_LO2 + CTX_SIZE)(s5)
	mfhi        s6, $ac1
	sw          s6, (DSPCTX_HI1 + CTX_SIZE)(s5)
	mflo        s6, $ac1
	sw          s6, (DSPCTX_LO1 + CTX_SIZE)(s5)


#endif

	/* Update the task stack pointer value if nesting is zero. */
	la          s6, uxInterruptNesting
	lw          s6, (s6)
	addiu       s6, s6, -1
	bne         s6, zero, 1f
	nop

	/* Save the stack pointer. */
	la          s6, uxSavedTaskStackPointer
	sw          s5, (s6)
1:
	.endm

/******************************************************************/

/* Restore the processor context. */
.macro  portRESTORE_CONTEXT

	/* Restore the stack pointer from the TCB.  This is only done if the
	nesting count is 1. */
	la          s6, uxInterruptNesting
	lw          s6, (s6)
	addiu       s6, s6, -1
	bne         s6, zero, 1f
	nop
	la          s6, uxSavedTaskStackPointer
	lw          s5, (s6)

	/* Restore the context. */
1:
#if (__mips_isa_rev < 6)
	lw          s6, CTX_LO0(s5)
	mtlo        s6
	lw          s6, CTX_HI0(s5)
	mthi        s6
#endif

#ifdef __mips_hard_float
	la			t0, currentfpctx
	lw			t1, (FPU_CTX + CTX_SIZE + DSPCTX_SIZE)(s5)
	sw			t1, (t0)
#endif

#ifdef __mips_dsp
/* Restore the DSP context */
	lw          s6, (DSPCTX_LO1 + CTX_SIZE)(s5)
	mtlo        s6, $ac1
	lw          s6, (DSPCTX_HI1 + CTX_SIZE)(s5)
	mthi        s6, $ac1

	lw          s6, (DSPCTX_LO2 + CTX_SIZE)(s5)
	mtlo        s6, $ac2
	lw          s6, (DSPCTX_HI2 + CTX_SIZE)(s5)
	mthi        s6, $ac2

	lw          s6, (DSPCTX_LO3 + CTX_SIZE)(s5)
	mtlo        s6, $ac3
	lw          s6, (DSPCTX_HI3 + CTX_SIZE)(s5)
	mthi        s6, $ac3

	lw          s6, (DSPCTX_DSPC + CTX_SIZE)(s5)
	rddsp       s6

#endif

	lw          $1, CTX_AT(s5)
	/* s6 is loaded as it was used as a scratch register and therefore saved
	as part of the interrupt context. */
	lw          s6, CTX_S6(s5)
	lw          v0, CTX_V0(s5)
	lw          v1, CTX_V1(s5)
	lw          a0, CTX_A0(s5)
	lw          a1, CTX_A1(s5)
	lw          a2, CTX_A2(s5)
	lw          a3, CTX_A3(s5)
	lw          t0, CTX_T0(s5)
	lw          t1, CTX_T1(s5)
	lw          t2, CTX_T2(s5)
	lw          t3, CTX_T3(s5)
	lw          t4, CTX_T4(s5)
	lw          t5, CTX_T5(s5)
	lw          t6, CTX_T6(s5)
	lw          t7, CTX_T7(s5)
	lw          t8, CTX_T8(s5)
	lw          t9, CTX_T9(s5)
	lw          s8, CTX_FP(s5)
	lw          ra, CTX_RA(s5)

	/* Protect access to the k registers, and others. */
	di
	ehb

	/* Decrement the nesting count. */
	la          k0, uxInterruptNesting
	lw          k1, (k0)
	addiu       k1, k1, -1
	sw          k1, 0(k0)

	lw          k0, CTX_STATUS(s5)
	lw          k1, CTX_EPC(s5)

	/* Leave the stack in its original state.  First load sp from s5, then
	restore s5 from the stack. */
	add         sp, zero, s5
	lw          s5, CTX_S5(sp)
	addiu       sp, sp, portCONTEXT_SIZE

	mtc0        k0, C0_STATUS
	mtc0        k1, C0_EPC
	ehb
	eret
	nop

	.endm

/******************************************************************/

	/* Yield context save */
	.macro portYIELD_SAVE

	/* Make room for the context. First save the current status so it can be
	manipulated. */
	addiu		sp, sp, -portCONTEXT_SIZE
	mfc0		k1, C0_STATUS
#ifdef __mips_hard_float
	/* disable FPU so that we can lazy context switch */
	ins			k1, zero, SR_CU1_SHIFT, 1
	mtc0        k1, C0_STATUS
	ehb
#endif

	/* Also save s6 and s5 so they can be used.  Any nesting interrupts should
	maintain the values of these registers across the ISR. */
	sw			s6, CTX_S6(sp)
	sw			s5, CTX_S5(sp)
	sw			k1, CTX_STATUS(sp)

	/* Prepare to re-enable the interrupts. */
	ins			k1, zero, 8, 8
	ori			k1, k1, ( 0x80 << 8 )
	ins			k1, zero, 1, 4          /* Clear EXL, ERL and UM. */

	/* s5 is used as the frame pointer. */
	add			s5, zero, sp

	/* Swap to the system stack.  This is not conditional on the nesting
	count as this interrupt is always the lowest priority and therefore
	the nesting is always 0. */
	la			sp, xISRStackTop
	lw			sp, (sp)

	/* Set the nesting count. */
	la			k0, uxInterruptNesting
	addiu		s6, zero, 1
	sw			s6, 0(k0)

	/* s6 holds the EPC value, this is saved with the rest of the context
	after interrupts are enabled. */
	mfc0		s6, C0_EPC

	/* Re-enable interrupts above configMAX_SYSCALL_INTERRUPT_PRIORITY. */
	mtc0		k1, C0_STATUS

	/* Save the context into the space just created.  s6 is saved again
	here as it now contains the EPC value. */
	sw			ra, CTX_RA(s5)
	sw			s8, CTX_FP(s5)
	sw			t9, CTX_T9(s5)
	sw			t8, CTX_T8(s5)
	sw			t7, CTX_T7(s5)
	sw			t6, CTX_T6(s5)
	sw			t5, CTX_T5(s5)
	sw			t4, CTX_T4(s5)
	sw			t3, CTX_T3(s5)
	sw			t2, CTX_T2(s5)
	sw			t1, CTX_T1(s5)
	sw			t0, CTX_T0(s5)
	sw			a3, CTX_A3(s5)
	sw			a2, CTX_A2(s5)
	sw			a1, CTX_A1(s5)
	sw			a0, CTX_A0(s5)
	sw			v1, CTX_V1(s5)
	sw			v0, CTX_V0(s5)
	sw			s7, CTX_S7(s5)
	sw			s6, CTX_EPC(s5)
	/* s5 and s6 has already been saved. */
	sw			s4, CTX_S4(s5)
	sw			s3, CTX_S3(s5)
	sw			s2, CTX_S2(s5)
	sw			s1, CTX_S1(s5)
	sw			s0, CTX_S0(s5)
	sw			$1, CTX_AT(s5)

	/* s7 is used as a scratch register as this should always be saved across
	nesting interrupts. */
#ifdef __mips_dsp
	/* Store the DSP context */

	rddsp       s7
	sw          s7, (DSPCTX_DSPC + CTX_SIZE)(s5)

	mfhi        s7, $ac3
	sw          s7, (DSPCTX_HI3 + CTX_SIZE)(s5)
	mflo        s7, $ac3
	sw          s7, (DSPCTX_LO3 + CTX_SIZE)(s5)
	mfhi        s7, $ac2
	sw          s7, (DSPCTX_HI2 + CTX_SIZE)(s5)
	mflo        s7, $ac2
	sw          s7, (DSPCTX_LO2 + CTX_SIZE)(s5)
	mfhi        s7, $ac1
	sw          s7, (DSPCTX_HI1 + CTX_SIZE)(s5)
	mflo        s7, $ac1
	sw          s7, (DSPCTX_LO1 + CTX_SIZE)(s5)
#endif

#ifdef __mips_hard_float
	la          s7, currentfpctx
	lw          s7, (s7)
	sw			s7, (FPU_CTX + CTX_SIZE + DSPCTX_SIZE)(s5)
#endif

#if (__mips_isa_rev < 6)
	mfhi		s7
	sw			s7, CTX_HI0(s5)
	mflo		s7
	sw			s7, CTX_LO0(s5)
#endif

	/* Save the stack pointer to the task. */
	la			s7, pxCurrentTCB
	lw			s7, (s7)
	sw			s5, (s7)

	/* Save the interrupt mask before the yield - this is the lowest priority
	in which API calls can be made */
	di
	ehb
	la			s7, EIC
	lw			s7, (s7)
	la			s6, GIC
	lw			s6, (s6)
	or			s7, s7, s6
	beqz		s7, 1f
	nop
	mfc0		s7, C0_STATUS
	ins			s7, zero, 10, 7
	ins			s7, zero, 18, 1
	ori			s6, s7, ( configMAX_API_CALL_INTERRUPT_PRIORITY << 10 ) | 1
	b			2f
	nop
1:
	mfc0		s7, C0_STATUS
	ins			s7, zero, 8, 8
	ori			s6, s7, ( 0x80 << 8 ) | 1

2:
	/* Clear the software interrupt in the core, so that we don't infinitely
	recurse. */
	mfc0		s6, C0_CAUSE
	ins			s6, zero, 8, 1
	mtc0		s6, C0_CAUSE
	ehb

	/* Re-enable interrupts */
	mtc0		s6, C0_STATUS
	ehb

	.endm

/******************************************************************/

	.macro portYIELD_RESTORE
	/* Clear the interrupt mask again.  The saved status value is still in s7. */
	mtc0		s7, C0_STATUS
	ehb

	/* Restore the stack pointer from the TCB. */
	la			s0, pxCurrentTCB
	lw			s0, (s0)
	lw			s5, (s0)

	/* Restore the rest of the context. */
#if (__mips_isa_rev < 6)
	lw			s0, CTX_LO0(s5)
	mtlo		s0
	lw			s0, CTX_HI0(s5)
	mthi		s0
#endif

#ifdef __mips_hard_float
	la			t0, currentfpctx
	lw			t1, (FPU_CTX+ CTX_SIZE + DSPCTX_SIZE)(s5)
	sw			t1, (t0)
#endif

#ifdef __mips_dsp
	/* Restore the DSP context */
	lw          s0, (DSPCTX_LO1 + CTX_SIZE)(s5)
	mtlo        s0, $ac1
	lw          s0, (DSPCTX_HI1 + CTX_SIZE)(s5)
	mthi        s0, $ac1

	lw          s0, (DSPCTX_LO2 + CTX_SIZE)(s5)
	mtlo        s0, $ac2
	lw          s0, (DSPCTX_HI2 + CTX_SIZE)(s5)
	mthi        s0, $ac2

	lw          s0, (DSPCTX_LO3 + CTX_SIZE)(s5)
	mtlo        s0, $ac3
	lw          s0, (DSPCTX_HI3 + CTX_SIZE)(s5)
	mthi        s0, $ac3

	lw          s0, (DSPCTX_DSPC + CTX_SIZE)(s5)
	rddsp       s0
#endif

	lw			$1, CTX_AT(s5)
	lw			s0, CTX_S0(s5)
	lw			s1, CTX_S1(s5)
	lw			s2, CTX_S2(s5)
	lw			s3, CTX_S3(s5)
	lw			s4, CTX_S4(s5)
	/* s5 is loaded later. */
	lw			s6, CTX_S6(s5)
	lw			s7, CTX_S7(s5)
	lw			v0, CTX_V0(s5)
	lw			v1, CTX_V1(s5)
	lw			a0, CTX_A0(s5)
	lw			a1, CTX_A1(s5)
	lw			a2, CTX_A2(s5)
	lw			a3, CTX_A3(s5)
	lw			t0, CTX_T0(s5)
	lw			t1, CTX_T1(s5)
	lw			t2, CTX_T2(s5)
	lw			t3, CTX_T3(s5)
	lw			t4, CTX_T4(s5)
	lw			t5, CTX_T5(s5)
	lw			t6, CTX_T6(s5)
	lw			t7, CTX_T7(s5)
	lw			t8, CTX_T8(s5)
	lw			t9, CTX_T9(s5)
	lw			s8, CTX_FP(s5)
	lw			ra, CTX_RA(s5)

	/* Protect access to the k registers, and others. */
	di
	ehb

	/* Set nesting back to zero.  As the lowest priority interrupt this
	interrupt cannot have nested. */
	la			k0, uxInterruptNesting
	sw			zero, 0(k0)

	/* Switch back to use the real stack pointer. */
	add			sp, zero, s5

	/* Restore the real s5 value. */
	lw			s5, CTX_S5(sp)

	/* Pop the status and epc values. */
	lw			k1, CTX_STATUS(sp)
	lw			k0, CTX_EPC(sp)

	/* Remove stack frame. */
	addiu		sp, sp, portCONTEXT_SIZE

	mtc0		k1, C0_STATUS
	mtc0		k0, C0_EPC
	ehb
	eret
	nop

	.endm

#endif

#endif	/* ISR_SUPPORT_H */
